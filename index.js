console.log('hello')


let firstName = document.querySelector('#txt-first-name');
let lastName = document.querySelector('#txt-last-name');
let spanFullName = document.querySelector('#span-full-name');


let updateFullName = (event) => {

	spanFullName.innerHTML = `${firstName.value} ${lastName.value}`;

};

firstName.addEventListener(`keyup`, updateFullName);
lastName.addEventListener(`keyup`, updateFullName);